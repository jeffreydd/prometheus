# Prometheus & Grafana

In CNO, [Prometheus](https://prometheus.io) and [Grafana](https://grafana.com/) are used for monitoring and alerting.

__Platform Administrators__ use these components to monitor the cluster and the platform components.

__Application Administrators__ use these components to monitor their applications.

# Contents

This project defines Ansible roles for deploying Prometheus and Grafana.

The Prometheus CRDs from this project are initially installed when bootstrapping a Kubernetes cluster with CNO.

Prerequisites for Prometheus CRDs:

- None

Prerequisites for Prometheus and Grafana:

- [CertManager](https://gitlab.com/logius/cloud-native-overheid/components/certmanager)
- [Nginx Ingress Controller](https://gitlab.com/logius/cloud-native-overheid/components/nginx-ingress)
- [Keycloak](https://gitlab.com/logius/cloud-native-overheid/components/keycloak)

In this project:

- Local installation, test and development of Prometheus CRDs (all optional)
- Local installation, test and development of Prometheus & Grafana (all optional)
- Datacenter installation
- Design notes

Local installation assumes the [platform-runner](https://gitlab.com/logius/cloud-native-overheid/components/platform-runner) to run in a Docker image.

# Local installation of Prometheus CRDs

Get platform runner:
```
docker pull registry.gitlab.com/logius/cloud-native-overheid/components/platform-runner:latest
```

Connect to Kubernetes cluster, e.g.:
```
export KUBECONFIG=$HOME/.kube/config
```

In working directory, add or extend `cluster_config.yaml`:
```
platform:
  prefix: "local"
  domain: example.com # Use your own domain, ref. certmanager

config:
  prometheus:
    create_custom_resource_defs: true
    remove_custom_resource_defs: true
```

From working directory, install CRDs; using a tag to install CRDs only:
```
# Prepare Ansible command, assuming above cluster_config.yaml will be mounted below
ANSIBLE_CMD="ansible-playbook run-role.yml \
  -e @/playbook/configfiles/cluster_config.yaml \
  -e galaxy_url=https://gitlab.com/logius/cloud-native-overheid/components/prometheus \
  -t install_crds \
  -vv"

# Run Ansible role, using ANSIBLE_CMD with mounted cluster config
docker run --rm \
  -v $KUBECONFIG:/home/builder/.kube/config \
  -v $PWD/cluster_config.yaml:/playbook/configfiles/cluster_config.yaml \
  -e ANSIBLE_FORCE_COLOR=true \
  --network=host \
  registry.gitlab.com/logius/cloud-native-overheid/components/platform-runner:latest \
  $ANSIBLE_CMD
```

The installation uses dependencies as defined in `./meta/main.yml`.

The reading order for Ansible parameterization is: `vars/main.yml`, overlayed with `$PWD/cluster_config.yaml`.

# Local test of Prometheus CRDs

Smoke test:
```
kubectl get crds | grep monitoring
```

# Local development of Prometheus CRDs

Checkout this project, e.g.:
```
git clone git@gitlab.com:logius/cloud-native-overheid/components/prometheus.git
```

Checkout projects for dependent roles as defined in `$PWD/prometheus/meta/main.yml`:
```
git clone git@gitlab.com:logius/cloud-native-overheid/components/common.git
```

Develop and install, this time with `ANSIBLE_ROLES_PATH` and `galaxy_url` referring to local directory:
```
export ANSIBLE_ROLES_PATH=$PWD

ANSIBLE_CMD="ansible-playbook run-role.yml \
  -e @/playbook/configfiles/cluster_config.yaml \
  -e galaxy_url=prometheus \
  -t install_crds \
  -vv"

docker run --rm \
  -v $KUBECONFIG:/home/builder/.kube/config \
  -v $PWD/cluster_config.yaml:/playbook/configfiles/cluster_config.yaml \
  -v $ANSIBLE_ROLES_PATH:/playbook/roles \
  -e ANSIBLE_FORCE_COLOR=true \
  --network=host \
  registry.gitlab.com/logius/cloud-native-overheid/components/platform-runner:latest \
  $ANSIBLE_CMD
```

Optional cleanup using:
```
ANSIBLE_CMD="ansible-playbook run-role.yml \
  -e @/playbook/configfiles/cluster_config.yaml \
  -e galaxy_url=prometheus \
  -t remove_crds \
  -vv"
```

__Attention__: avoid the cleanup when you are about to install other CNO components subsequently.

# Local installation, test and development of Prometheus & Grafana

Same as above, this time without` -t install_crds` and `-t remove_crds`.

# Datacenter installation

In your own CI/CD, create a pipeline similar to the local installation.

Use a specific tag for `platform-runner`.

# Design notes

## Grafana plugins

To install Grafana plugins you need to add the relevant plugin name (i.e. `grafana-polystat-panel`) to the platform config file under:

```yaml
config:
  prometheus:
    grafana_plugins:
      - name: "grafana-piechart-panel"
```

#### Keyhub

To be able to link the prometheus component with OIDC There is an option to setup OIDC for prometheus using Keyhub. Prometheus/Grafana is by default configured to use keycloak, the default configuration can be found in path __vars/main.yml__.

Configuration:
```
config:
  prometheus:
    identity_provider: "keycloak"
```
To use Keyhub as IDP you have to set the following required settings in your cluster config:

* `identity_provider: "keyhub"`
* `config.system.identity_provider_url`  

.Note: this will overwrite the default setting in __vars/main.yml__.

Setting of hostmachine:

Make sure the following env are set on the hostmachine to identify with keyhub:

* `KEYHUB_OIDC_GRAFANA_CLIENT_ID`  
* `KEYHUB_OIDC_GRAFANA_CLIENT_SECRET`

OpenID Connect Scopes:

the following scopes are used by Prometheus/Grafana during authentication to authorize access to a user's details:

```yaml
scopes:
  - openid
  - email
  - profile
```
