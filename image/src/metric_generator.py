#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This module Generates a bunhc of random metrics for tests
"""

import logging
import random
import time
from prometheus_client import start_http_server, Gauge  # type: ignore

if __name__ == '__main__':
    start_http_server(5000)

    LOGGER = logging.getLogger(__name__)

    CONTENT_TYPE_LATEST = str('text/plain; version=0.0.4; charset=utf-8')

    # Define the different gauges
    GAUGE_TESTS = Gauge('prometheus_test_gen_metrics', 'Generated metrics for prometheus tests', ['tests_for'])

    # go into while loop
    while True:
        logging.warning("========= Starting Filling Gauges =========")
        test_metric = random.randint(0, 20)
        print(test_metric)
        logging.warning(f"Current value: {test_metric}")

        GAUGE_TESTS.labels("prometheus-test-metrics").set(test_metric)
        time.sleep(10)
