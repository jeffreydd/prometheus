---
## Alertmanager - https://prometheus.io/docs/alerting/alertmanager/
alertmanager:
  enabled: false
  ingress:
    annotations:
      kubernetes.io/ingress.class: nginx
      cert-manager.io/cluster-issuer: {{ config.certmanager.issuer | default('none') }}
    hosts:
    - "{{ config.prometheus.alertmanager_fqdn }}"
    tls:
    - hosts:
      - "{{ config.prometheus.alertmanager_fqdn }}"
      secretName: "{{ config.prometheus.alertmanager_tls_secret_name | default() }}"
  alertmanagerSpec:
    storage:
      volumeClaimTemplate:
        spec:
          storageClassName: "{{ config.system.storage_class_name | default() }}"
    externalUrl: "{{ config.prometheus.alertmanager_fqdn }}"

# Grafana - https://github.com/helm/charts/blob/master/stable/grafana/values.yaml
grafana:
  rbac:
    pspUseAppArmor: false
  ## For PVC ReadWriteOnce use Deployment strategy Recreate, default is RollingUpdate
  ## ref: https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#strategy
  deploymentStrategy:
    type: "{{ config.prometheus.deployment_strategy_type | default('RollingUpdate') }}"
  enabled: {{ config.prometheus.grafana_enabled | default(true) }}
  adminPassword: "{{ grafana_admin_password }}"
  persistence:
    enabled: true
    type: pvc
    storageClassName: "{{ config.system.storage_class_name | default() }}"
  initChownData:
    enabled: false
  grafana.ini:
    server:
      domain: "{{ config.prometheus.grafana_fqdn }}"
      root_url: "https://{{ config.prometheus.grafana_fqdn }}"
    auth.generic_oauth:
      enabled: true
      client_id: {{ client_id }}
      client_secret: {{ oidc_client_secret }}
      scopes: openid email profile
      auth_url: {{ auth_url }}
      token_url: {{ token_url }}
      api_url: {{ api_url }}
      allow_sign_up: true
      tls_skip_verify_insecure: true  # Remove when keycloak has acceptable certs
  ingress:
    enabled: {{ config.prometheus.grafana_enabled | default(true) }}
    annotations:
      kubernetes.io/ingress.class: nginx
      cert-manager.io/cluster-issuer: {{ config.certmanager.issuer | default('none') }}
    hosts:
    - "{{ config.prometheus.grafana_fqdn }}"
    tls:
    - hosts:
      - "{{ config.prometheus.grafana_fqdn }}"
      secretName: "{{ config.prometheus.grafana_tls_secret_name | default() }}"

nodeExporter:
  enabled: {{ config.prometheus.nodeExporter_enabled | default(true) }}
# The mounstat NFS collector collects too much and not very well
# labelled metrics. This config drops some metrics and relabels the rest.
{% if config.prometheus.enable_mountstat_monitoring == True %}
  serviceMonitor:
    metricRelabelings:
      - sourceLabels: [__name__]
        # Drop all event and transport metrics
        regex: ^node_mountstats_nfs_(event|transport)_.+
        replacement: $1
        action: drop

        # The label 'export=<ip>:/mnt/<share>/<ns>+<pod>+<pvc-uuid>'
        # We want to split this in share, nfs_ns, nfs_pod, nfs_pvc
        # Actually the label is not relabelled but copies into a new one.

        # We ASUME that namespaces are according namingconvention
        # l12m-<name> and we are aware that this is not always
        # the case (eg. l12m-backup-vault).
        # For nicer labeling we have to change these namespace names.

      - sourceLabels: [export]
        action: replace
        regex: '\d+.\d+.\d+.\d+:\/\w+\/(\w+-?\w+)\/.*'
        replacement: $1
        targetLabel: share

      - sourceLabels: [export]
        action: replace
        regex: '\d+.\d+.\d+.\d+:\/\w+\/\w+-?\w+\/(\w+-\w+).*'
        replacement: $1
        targetLabel: nfs_ns

      - sourceLabels: [export]
        action: replace
        regex: '\d+.\d+.\d+.\d+:\/\w+\/\w+-?\w+\/\w+-\w+-(.+)-pvc-.+'
        replacement: $1
        targetLabel: nfs_pvcname

      - sourceLabels: [export]
        action: replace
        regex: '\d+.\d+.\d+.\d+:\/\w+\/\w+-?\w+\/\w+-\w+-.+-(pvc-.+)'
        replacement: $1
        targetLabel: nfs_pvc
{% endif %}


# Node exporter
# Cloud Provide might have a Prometheus running on 9100. We choose another port.
prometheus-node-exporter:
# There is a problem with the images for node-exporter > 1.0.0 in our infrastructure
# and mountstat (nfs) metrics. As work around we will use the last known working image
  service:
    port: 9101
    targetPort: 9101
  extraHostVolumeMounts: {{ enable_node_exporter_rootfs | default() }}
  extraArgs: {{ enable_node_exporter_mountstat | default(node_exporter_default_extra_args) }}
{% if config.prometheus.enable_node_exporter_on_master | default(false) == false %}
  tolerations: {}
{% endif %}

## Prometheus Operator
prometheusOperator:
  # CRDs must be installed before the helm chart runs (https://github.com/helm/helm/issues/6130)
  createCustomResource: {{ config.prometheus.create_custom_resource_defs | default(false) }}
  ## Namespaces to scope the interaction of the Prometheus Operator and the apiserver (allow list).
  ## This is mutually exclusive with denyNamespaces. Setting this to an empty object will disable the configuration
  namespaces: {{ config.prometheus.namespaces | default() }}
  ## Namespaces not to scope the interaction of the Prometheus Operator (deny list).
  # Exclude namespace kube-system and in case of an Openshift cluster exclude openshift-monitoring too.
  denyNamespaces: "{{ config.prometheus.deny_namespaces | default( (config.system.k8s_distribution == 'openshift' | default(false)) | ternary('openshift-monitoring,kube-system', 'kube-system') ) }}"

  ## If true, the operator will create and maintain a service for scraping kubelets
  ## ref: https://github.com/coreos/prometheus-operator/blob/master/helm/prometheus-operator/README.md
  ##
  kubeletService:
    enabled: {{ config.prometheus.kubelet_service_enabled | default((config.system.k8s_distribution != 'openshift') | default(true)) }}
    namespace: kube-system



## Prometheus
prometheus:
  ingress:
    enabled: true
    annotations:
      kubernetes.io/ingress.class: nginx
      cert-manager.io/cluster-issuer: {{ config.certmanager.issuer | default('none') }}
    hosts:
    - "{{ config.prometheus.prometheus_fqdn }}"
    tls:
    - hosts:
      - "{{ config.prometheus.prometheus_fqdn }}"
      secretName: "{{ config.prometheus.prometheus_tls_secret_name | default() }}"
  persistence:
    enabled: true
    type: pvc
  prometheusSpec:
    storageSpec:
      volumeClaimTemplate:
        spec:
          accessModes: ["ReadWriteOnce"]
          resources:
            requests:
              storage: 10Gi
    retention: "{{ config.prometheus.storageSpec.retention | default('7d') }}"

## Component scraping coreDns. Use either this or kubeDns
##
coreDns:
  enabled: {{ config.prometheus.core_dns_enabled | default((config.system.k8s_distribution != 'openshift') | default(true)) }}

## Component scraping the kubelet and kubelet-hosted cAdvisor
##
kubelet:
  enabled: {{ config.prometheus.kubelet_enabled | default((config.system.k8s_distribution != 'openshift') | default(true)) }}

## Component scraping the kube controller manager
##
kubeControllerManager:
  enabled: {{ config.prometheus.kube_controller_enabled | default((config.system.k8s_distribution != 'openshift') | default(true)) }}

## Component scraping etcd
##
kubeEtcd:
  enabled: {{ config.prometheus.kube_etcd_enabled | default((config.system.k8s_distribution != 'openshift') | default(true)) }}

## Component scraping kube proxy
##
kubeProxy:
  enabled: {{ config.prometheus.kube_proxy_enabled | default((config.system.k8s_distribution != 'openshift') | default(true)) }}

## Component scraping kube scheduler
##
kubeScheduler:
  enabled: {{ config.prometheus.kube_scheduler_enabled | default((config.system.k8s_distribution != 'openshift') | default(true)) }}
